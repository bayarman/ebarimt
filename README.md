# EBarimt

# Тохиргоо

1. `data` folder үүсгэх
2. `poslib` folder үүсгэх
3. `poslib` дотор Татвараас ирсэн `.so` өргөтгөлтэй файлыг `libPosAPI.so` нэртэй хадгална.

Using docker

```
docker run -d \
  -p 8001:5000 \
  -v "$(pwd)"/data:/root/.vatps \
  -v "$(pwd)"/poslib:/poslib:ro \
  --name ebarimt registry.gitlab.com/endigo/ebarimt:latest
```

эсвэл

Using docker-compose
`docker-compose.yml` файл үүсгэнэ

```
version: "2"
services:
  web:
    image: registry.gitlab.com/endigo/ebarimt:latest
    ports:
      - 8000:5000
    restart: always
    volumes:
      - ./data:/root/.vatps
      - ./poslib:/poslib
```

Next step

```
docker-compose up -d
```

# Routes

1. `/health` - Сервис хэвийн ажиллаж байгаа эсэхийг шалгана.
2. `/checkApi` - POS API ажиллаж байгааг эсэхийг шалгана.

```
# Response
{
  "config": {
    "success": true
  },
  "database": {
    "success": true
  },
  "network": {
    "success": true
  },
  "success": true
}
```

3. `/getInformation` -

4. `/callFunction` -
5. `/put` - Баримт хэвлэх үед ашиглана
6. `/returnBill` - Баримт буцаах
7. `/sendData` - Баримтуудыг илгээх, шинэ сугалааны дугаар явуулахад ашиглана. 24 цагт 1 удаа хамгийн багадаа ажиллаж ёстой
